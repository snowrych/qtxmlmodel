import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.3

/* custom class import */
import Xml.Manager 1.0

ApplicationWindow{
    id: window
    visible: true

    maximumHeight : 600
    maximumWidth :  800
    minimumHeight : maximumHeight / 3
    minimumWidth : maximumWidth / 2
    menuBar:myMenu
    title: qsTr("ModelConstruct v0.1")
    property bool fileLoaded: false
    property int itemId: 0
    Component.onCompleted: {
        window.setWidth(maximumWidth)
        window.setHeight(maximumHeight / 1.25)

    }
    //    XmlManager{
    //        id:xmlManager
    //    }

    //    TestArea{
    //        id:testArea
    //    }
    CustomMenu{
        id:myMenu
        width: parent.width
    }



    /* components */




    Drawer {
        id: expanderMenu
        width: window.width/3.5
        height: window.height
        onClosed: dropArea.anchors.leftMargin = 0
        onOpened: dropArea.anchors.leftMargin = expanderMenu.width
        Button{
            id:closeButton
            onClicked: expanderMenu.close()
            icon.source: "qrc:/IconSource/clear.png"
            anchors.right: parent.right
            width:28
            height: width
        }
        /*model */
        ListView{
            id: dragList
            anchors {
                left: parent.left
                right: parent.right
                top: closeButton.bottom
                bottom: parent.bottom
            }

            model: myModel

            delegate:  Component {
                id: delegateComponent
                Loader {
                    sourceComponent: Component {
                        Rectangle {
                            id:itemDelegate
                            Row{
                                anchors.fill:parent
                                Text{text: modelData.name}
                                Image {width:25;height:25;source:modelData.source}
                                spacing: 5
                                MouseArea {
                                    id:dragArea
                                    cursorShape : Qt.ClosedHandCursor
                                    anchors.fill:parent
                                    onPressed:{
                                        dragList.currentIndex = index
                                        dragArea.drag.target = dragList.currentItem
                                        dropArea.obj = dragList.currentItem
                                    }
                                }
                            }
                            width: expanderMenu.width
                            height: 25
                            border.color: "black"
                            border.width: 0.5
                            color: ListView.isCurrentItem ? "green":"transparent"

                        }
                    }
                }
            }
            //                Rectangle {
            //                id:item
            //                Row{
            //                    anchors.horizontalCenter: parent.horizontalCenter
            //                    anchors.verticalCenter: parent.verticalCenter
            //                    spacing: 5
            //                    Text{text: modelData.name}
            //                    Image {width:25;height:25;source:modelData.source}
            //                }
            //                width: parent.width
            //                height: 25
            //                border.color: "black"
            //                border.width: 0.5
            //                color: ListView.isCurrentItem ? "green":"transparent"
            //                MouseArea {
            //                    id:dragArea
            //                    anchors.fill: parent
            //                    drag.axis: Drag.XAndYAxis - item.scale
            //                    cursorShape : Qt.ClosedHandCursor
            //                    onPressed:{
            //                        dragList.currentIndex = index
            //                        dragArea.anchors.fill = dragList.currentItem
            //                        dragArea.drag.target = dragList.currentItem

            //                        dropArea.selection =
            //                                Component.createObject
            //                    }
            //                }
            //            }

        }
    }

    /* Сюда бросаем */

    Rectangle {
        id: dropArea

        property var obj

        color: "transparent"
        anchors {
            left: parent.left
            right: parent.right
            top: myMenu.bottom
            bottom: parent.bottom
            bottomMargin: window.width /3
            rightMargin: window.width /4
        }
        border.color: "black"
        border.width: 1
        ListView{
            id:dropList
            model:dragList
            delegate:  dropArea.obj
            MouseArea{
                id:dropSource
                anchors.fill: parent
                //            drag.target:rect
                //            cursorShape : Qt.DragCopyCursor
                //            drag.
            }
        }
        Label {
            id:placeHolderText
            enabled: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Перетащите сюда ") + dropArea.obj
            font.pixelSize : 12
        }

    }

    /* Диалоговые окна */
    Dialog {
        id:aboutProgramm

        title:qsTr("О программе")
        //        informativeText : qsTr("Программа для визуального конструирования моделей")
        //        detailedText : qsTr("тут кто-то был...")
        Label{
            text:  qsTr("Программа для визуального конструирования моделей")
        }
        standardButtons: StandardButton.Ok
        Component.onCompleted: visible = false
        onRejected: aboutProgramm.close()
    }
}
