import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4

MenuBar {
    id:toolMenu
    Menu {
        title: qsTr("Файл")
        implicitHeight: contentHeight
        Action { text: qsTr("Новый..."); onTriggered:testArea.show();icon.source: "qrc:/IconSource/clear.png" }
        Action { text: qsTr("Открыть...");icon.source: "qrc:/IconSource/open.png" }
        Action { text: qsTr("Сохранить");icon.source: "qrc:/IconSource/save.png" }
        Action { text: qsTr("Сохранить Как...");icon.source: "qrc:/IconSource/save.png"}
        MenuSeparator {}
        Action { text: qsTr("Выход");onTriggered: Qt.quit() }
    }
    Menu {
        title: qsTr("К&онструктор")
       implicitHeight: contentHeight
        Action { text: qsTr("Инструменты");onTriggered:expanderMenu.open();icon.source: "qrc:/IconSource/expanderMenu.png"}
        Action { text: qsTr("Очистить")/*;onTriggered:dropList.clear()*/;icon.source: "qrc:/IconSource/remove.png" }
        Action { text: qsTr("Скопировать в буфер") }
        Action { text: qsTr("Вставить") }
    }
    Menu {
        implicitHeight: contentHeight
        title: qsTr("Помощь")
        Action { text: qsTr("О программе");onTriggered: aboutProgramm.open();icon.source:"qrc:/IconSource/info.png"}
    }


    font.pixelSize: 11


    //Rectangle{color: "#FFC300";border.color: "black"}
}

