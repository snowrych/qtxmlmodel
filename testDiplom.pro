CONFIG += c++11

QT += quick widgets xml

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
    ItemClasses/DataObject.cpp \
    ItemClasses/DataObjectListModel.cpp \
    ItemClasses/QuadraticFunction.cpp \
    Utility/XmlManager.cpp \
        main.cpp \

RESOURCES += \
    icons.qrc \
    qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ItemClasses/DataObject.h \
    ItemClasses/DataObjectListModel.h \
    ItemClasses/QuadraticFunction.h \
    Utility/SingletonMacros.h \
    Utility/XmlManager.h

DISTFILES +=


