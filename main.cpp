#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQuick>
#include <QQuickView>

#include <QIcon>
#include <QtDebug>
#include <QSize>
#include <QStringList>

#include "Utility/XmlManager.h"
#include "ItemClasses/DataObjectListModel.h"

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QIcon appIcon(":/IconSource/main_title.svg");
    app.setWindowIcon(appIcon);

    qmlRegisterType<XmlManager>("Xml.Manager",1,0,"XmlManager");

    QQmlApplicationEngine engine;

    DataObjectListModel listModel;

    engine.rootContext()->setContextProperty("myModel",  QVariant::fromValue(listModel.datalist()));
    engine.load(QUrl(QLatin1String("qrc:/QmlPages/main.qml")));


    qDebug() << "counts delegate:" << listModel.datalist().count();
    return app.exec();

}
