#include "QuadraticFunction.h"

QuadraticFunction::QuadraticFunction()
{

}

QString QuadraticFunction::functionName() const
{
    return m_functionName;
}

void QuadraticFunction::setFunctionName(const QString &functionName)
{
    m_functionName.clear();
    m_functionName = functionName;
}

QuadraticFunction::InputData QuadraticFunction::getParammetrs() const
{
    return inputData;
}

void QuadraticFunction::setParammetrs(const double &value1,const double &value2,const double &value3)
{
    inputData.a = value1;
    inputData.b = value2;
    inputData.c = value3;

}
