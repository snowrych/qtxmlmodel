#pragma once

#include <QObject>

class DataObject : public QObject {
    Q_OBJECT
    Q_PROPERTY(quint16 id READ id WRITE  setId NOTIFY propertyChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY propertyChanged)
    Q_PROPERTY(QString source READ source WRITE setSource  NOTIFY propertyChanged)

public:

    DataObject(QObject *parent = nullptr);
    DataObject(quint16 id,QString name,QString source);
    DataObject(const DataObject &refference);


    QString name() const;
    void setName(const QString &name);


    quint16 id() const;
    void setId(const quint16 &id);

    QString source() const;
    void setSource(const QString &source);

signals:
    void propertyChanged();

private:
    QString m_name;
    QString m_source;
    quint16 m_id;
};
Q_DECLARE_METATYPE(DataObject);
