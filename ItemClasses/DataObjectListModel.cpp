#include "DataObjectListModel.h"

DataObjectListModel::DataObjectListModel()
{
    fillList();
}

void DataObjectListModel::fillList()
{
    m_datalist.clear();
    m_datalist.append(new DataObject(0,"Item 1", "qrc:/IconSource/algorithm.png"));
    m_datalist.append(new DataObject(1,"Item 2", "qrc:/IconSource/func.png"));
    m_datalist.append(new DataObject(0,"Item 3", "qrc:/IconSource/algorithm.png"));
    m_datalist.append(new DataObject(1,"Item 4", "qrc:/IconSource/func.png"));
    m_datalist.append(new DataObject(1,"Item 5", "qrc:/IconSource/func.png"));
}

QList<QObject *> DataObjectListModel::datalist() const
{
    return m_datalist;
}

void DataObjectListModel::setObjectToModel(const quint16  &id,const QString &name, const QString &source)
{
    datalist().append(new DataObject(id,name,source));
}

void DataObjectListModel::setDatalist(const QList<QObject *> &datalist)
{
    m_datalist = datalist;
}
