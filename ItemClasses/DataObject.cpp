#include "DataObject.h"

DataObject::DataObject(QObject *parent): QObject(parent)
{

}

DataObject::DataObject(quint16 id ,QString name, QString source)
{
    setId(id);
    setName(name);
    setSource(source);
}

DataObject::DataObject(const DataObject &refference) : QObject(),
    m_id(refference.m_id),
    m_name(refference.m_name),
    m_source(refference.m_source)

{

}

QString DataObject::name() const
{
    return m_name;
}

void DataObject::setName(const QString &name)
{
    m_name = name;
}

quint16 DataObject::id() const
{
    return m_id;
}

void DataObject::setId(const quint16 &id)
{
    m_id = id;
}

QString DataObject::source() const
{
    return m_source;
}

void DataObject::setSource(const QString &source)
{
    m_source = source;
}



