#pragma once

#include <QList>
#include <QObject>

#include <ItemClasses/DataObject.h>

class DataObjectListModel
{
public:
    DataObjectListModel();
    void fillList();
    QList<QObject*> datalist() const;
    void setObjectToModel(const quint16 &id, const QString &name, const QString &source);
    void setDatalist(const QList<QObject*> &datalist);

private:
    QList<QObject*> m_datalist;
};

