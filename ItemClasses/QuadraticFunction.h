#pragma once
#include <QObject>
#include <QString>
#include <QList>

class QuadraticFunction : public QObject {
    Q_OBJECT
public:
    QuadraticFunction();
private:
    struct InputData
       {
           double a = 0.0;
           double b = 0.0;
           double c = 0.0;
       } inputData;
public:

    QString functionName() const;
    void setFunctionName(const QString &functionName);

    InputData getParammetrs() const;
    void setParammetrs(const double &value1, const double &value2, const double &value3);

private:
    QString m_functionName="default";

};


