#pragma once
#include <QString>
#define SINGLETON(x) static x& instance() { static x s; return s; }
