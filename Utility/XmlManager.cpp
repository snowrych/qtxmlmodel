#include "XmlManager.h"

XmlManager::XmlManager()
{
}

void XmlManager::makeTestXML()
{
    QuadraticFunction function;
    function.setFunctionName("QFunction");
    function.setParammetrs(2.00,3.01,4.05);

    QDomDocument doc (function.functionName());
    QDomElement rootElement = doc.createElement(function.functionName());
    doc.appendChild(rootElement);

    /* добавляем к корневому элементу) */
    /* ax^2*/
    QDomElement firstRootChild = doc.createElement("x2");
    QDomText firstChildData = doc.createTextNode(QString::number(function.getParammetrs().a));
    firstRootChild.appendChild(firstChildData);
    rootElement.appendChild(firstRootChild);
    /* bx*/
    QDomElement secondRootChild = doc.createElement("x");
    QDomText secondChildData = doc.createTextNode(QString::number(function.getParammetrs().b));
    secondRootChild.appendChild(secondChildData);
    rootElement.appendChild(secondRootChild);
    /* c */
    QDomElement thirdRootChild = doc.createElement("const");
    QDomText thirdChildData = doc.createTextNode(QString::number(function.getParammetrs().c)); // QDomNode::NodeType
    thirdRootChild.appendChild(thirdChildData);
    rootElement.appendChild(thirdRootChild);

    /* QIODevice flags
    QIODevice::WriteOnly - перепишет первые 10 байт файла, остальные оставит как есть, размер файла будет 100 байт.
    QIODevice::WriteOnly|QIODevice::Truncate - обнулит содержимое файла, запишет 10 байт, размер файла будет 10 байт.
    QIODevice::WriteOnly|QIODevice::Append - запишет 10 байт в конец файла, размер файла будет 110 байт.
    */

    QFile file ("C:/Users/kolpakov/Desktop/NewTry/Samples/testFunc.xml");
    if (file.open(QFile::WriteOnly | QFile::Truncate)) {
        QTextStream out(&file);
        doc.save(out,4);
    }
    file.close();
}

void XmlManager::openTestXML()
{
    QDomDocument domDoc;
    QFile file ("C:/Users/kolpakov/Desktop/NewTry/Samples/testFunc.xml");
    if(file.open(QIODevice::ReadOnly)) {
        if(domDoc.setContent(&file)) {
            QDomElement domElement = domDoc.documentElement();
            travelFirsNode(domElement);
        }
        file.close();
    }
}

void XmlManager::travelFirsNode(const QDomNode &node)
{
    QDomNode domNode = node.firstChild();
    while (!domNode.isNull() ) {
        if(domNode.isElement()) {
            QDomElement domElement = domNode.toElement();
            if (!domElement.isNull () ) {
                if (domElement.tagName() == "Name"  ) {
                    qDebug () << "Attr: "
                              << domElement. attribute ("number", "");
                } else {
                    qDebug () << "TagName: " << domElement.tagName ()
                              << "\tText: "<< domElement.text();
                }
            }
        }
        travelFirsNode(domNode);
        domNode = domNode.nextSibling();
    }
//    if(node.hasChildNodes())
}

