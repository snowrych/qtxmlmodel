#pragma once

#include <QObject>
#include <QString>
#include <QDebug>
#include <QFile>
//#include <QXmlStreamWriter>
//#include <QXmlStreamAttribute>
#include <QXmlStreamReader>
#include <QtXml>
#include <QDomDocument>
#include <QTextStream>

#include "ItemClasses/QuadraticFunction.h"

class XmlManager : public QObject {
    Q_OBJECT
public:
    XmlManager();
    Q_INVOKABLE void makeTestXML();
    Q_INVOKABLE void openTestXML();

    void travelFirsNode(const QDomNode &node);


};

